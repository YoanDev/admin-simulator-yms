import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import {
  bgFooterContext,
  bgLayoutContext,
  bgNavContext,
  colorFooterContext,
  colorLayoutContext,
  colorNavContext,
} from "./hook/MyContext";
import {
  useColorNavReducer,
  useBgNavReducer,
  useBgLayoutReducer,
  useColorLayoutReducer,
  useBgFooterReducer,
  useColorFooterReducer,
} from "./hook/MyReducer";
import About from "./pages/About";
import Home from "./pages/Home";

function App() {
  const [state, dispatch] = useBgNavReducer();
  const [state2, dispatch2] = useColorNavReducer();
  const [state3, dispatch3] = useBgLayoutReducer();
  const [state4, dispatch4] = useColorLayoutReducer();
  const [state5, dispatch5] = useBgFooterReducer();
  const [state6, dispatch6] = useColorFooterReducer();
  return (
    <>
      <bgNavContext.Provider value={{ state, dispatch }}>
        <colorNavContext.Provider value={{ state2, dispatch: dispatch2 }}>
          <bgLayoutContext.Provider value={{ state3, dispatch: dispatch3 }}>
            <colorLayoutContext.Provider value={{ state4, dispatch: dispatch4 }}>
              <bgFooterContext.Provider value={{ state5, dispatch: dispatch5 }}>
                <colorFooterContext.Provider value={{ state6, dispatch: dispatch6 }}>
                  <Router>
                    <Switch>
                      <Route path="/" exact component={Home} />
                      <Route path="/about" exact component={About} />
                    </Switch>
                  </Router>
                </colorFooterContext.Provider>
              </bgFooterContext.Provider>
            </colorLayoutContext.Provider>
          </bgLayoutContext.Provider>
        </colorNavContext.Provider>
      </bgNavContext.Provider>
    </>
  );
}

export default App;
