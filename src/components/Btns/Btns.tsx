import { useContext } from 'react'
import { bgFooterContext, bgLayoutContext, bgNavContext, colorFooterContext, colorLayoutContext, colorNavContext } from '../../hook/MyContext'
import "./Btns.scss"

interface Props {
    action: string,
    text?: string
}

export const BtnNavBg = ({action, text}: Props) => {
    const {dispatch} = useContext(bgNavContext)
    return (
        <>
            <div className="btn" onClick={()=> dispatch({type: action})}>
                {text ? text : <div>button</div>}
            </div>
        </>
    )
}

export const BtnNavColor = ({action, text}: Props) => {
    const {dispatch} = useContext(colorNavContext)
    return (
        <>
            <div className="btn" onClick={()=> dispatch({type: action})}>
                {text ? text : <div>button</div>}
            </div>
        </>
    )
}


export const BtnBgColor = ({action, text}: Props) => {
    const {dispatch} = useContext(bgLayoutContext)
    return (
        <>
            <div className="btn" onClick={()=> dispatch({type: action})}>
                {text ? text : <div>button</div>}
            </div>
        </>
    )
}



export const BtnLayoutColor = ({action, text}: Props) => {
    const {dispatch} = useContext(colorLayoutContext)
    return (
        <>
            <div className="btn" onClick={()=> dispatch({type: action})}>
                {text ? text : <div>button</div>}
            </div>
        </>
    )
}



export const BtnLayoutBg = ({action, text}: Props) => {
    const {dispatch} = useContext(bgLayoutContext)
    return (
        <>
            <div className="btn" onClick={()=> dispatch({type: action})}>
                {text ? text : <div>button</div>}
            </div>
        </>
    )
}



export const BtnFooterBg = ({action, text}: Props) => {
    const {dispatch} = useContext(bgFooterContext)
    return (
        <>
            <div className="btn" onClick={()=> dispatch({type: action})}>
                {text ? text : <div>button</div>}
            </div>
        </>
    )
}



export const BtnFooterColor = ({action, text}: Props) => {
    const {dispatch} = useContext(colorFooterContext)
    return (
        <>
            <div className="btn" onClick={()=> dispatch({type: action})}>
                {text ? text : <div>button</div>}
            </div>
        </>
    )
}

