import { BtnFooterBg } from "../../Btns/Btns";

const FooterBg = () => {
  return (
    <>
      <div className="flex">
        <h3>Change Footer Background</h3>
        <div className="btn-wrapper">
          <BtnFooterBg action="SET_BFC_WHITE" text="White" />
          <BtnFooterBg action="SET_BFC_YELLOW" text="Yellow" />
          <BtnFooterBg action="SET_BFC_RED" text="Red" />
          <BtnFooterBg action="SET_BFC_BLUE" text="Blue" />
          <BtnFooterBg action="SET_BFC_GREY" text="Grey" />
          <BtnFooterBg action="SET_BFC_BLACK" text="Black" />
          <BtnFooterBg action="SET_BFC_PINK" text="Pink" />
          <BtnFooterBg action="SET_BFC_VIOLET" text="Violet" />
          <BtnFooterBg action="SET_BFC_BROWN" text="Brown" />
        </div>
      </div>
    </>
  );
};

export default FooterBg;
