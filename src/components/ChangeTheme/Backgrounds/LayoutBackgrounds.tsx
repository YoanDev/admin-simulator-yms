import { BtnLayoutBg } from "../../Btns/Btns";
import "../ChangeBg.scss"

const LayoutBg = () => {
  return (
    <>
      <div className="flex">
        <h3>Change Layout Background</h3>
        <div className="btn-wrapper">
          <BtnLayoutBg action="SET_LYB_WHITE" text="White" />
          <BtnLayoutBg action="SET_LYB_YELLOW" text="Yellow" />
          <BtnLayoutBg action="SET_LYB_RED" text="Red" />
          <BtnLayoutBg action="SET_LYB_BLUE" text="Blue" />
          <BtnLayoutBg action="SET_LYB_GREY" text="Grey" />
          <BtnLayoutBg action="SET_LYB_BLACK" text="Black" />
          <BtnLayoutBg action="SET_LYB_PINK" text="Pink" />
          <BtnLayoutBg action="SET_LYB_VIOLET" text="Violet" />
          <BtnLayoutBg action="SET_LYB_BROWN" text="Brown" />
        </div>
      </div>
    </>
  );
};

export default LayoutBg;
