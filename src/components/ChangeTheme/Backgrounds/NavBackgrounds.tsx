import { BtnNavBg } from "../../Btns/Btns";
import "../ChangeBg.scss"

const NavBackgrounds = () => {
  return (
    <>
      <div className="flex">
        <h3>Change Navbar Background</h3>
        <div className="btn-wrapper">
          <BtnNavBg action="SET_WHITE" text="White" />
          <BtnNavBg action="SET_YELLOW" text="Yellow" />
          <BtnNavBg action="SET_RED" text="Red" />
          <BtnNavBg action="SET_BLUE" text="Blue" />
          <BtnNavBg action="SET_GREY" text="Grey" />
          <BtnNavBg action="SET_BLACK" text="Black" />
          <BtnNavBg action="SET_PINK" text="Pink" />
          <BtnNavBg action="SET_VIOLET" text="Violet" />
          <BtnNavBg action="SET_BROWN" text="Brown" />
        </div>
      </div>
    </>
  );
};

export default NavBackgrounds;
