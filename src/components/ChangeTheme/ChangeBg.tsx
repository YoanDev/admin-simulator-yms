import "./ChangeBg.scss";
import { FiEdit } from "react-icons/fi";
import { AiOutlineArrowLeft } from "react-icons/ai";
import { useState } from "react";
import NavBackgrounds from "./Backgrounds/NavBackgrounds";
import NavColor from "./Colors/NavColor";
import {DiAptana} from 'react-icons/di'
import LayoutColor from "./Colors/LayoutColor";
import LayoutBg from "./Backgrounds/LayoutBackgrounds";
import FooterColor from "./Colors/FooterColor";
import FooterBg from "./Backgrounds/FooterBg";



const ChangeBg = () => {
  const [navbg, setNavBg] = useState(false);
  const [colorbg, setColorbg] = useState(false);
  const [layoutBg, setLayoutBg] = useState(false);
  const [layoutColor, setLayoutColor] = useState(false);
  const [footerBg, setFooterBg] = useState(false);
  const [footerColor, setFooterColor] = useState(false);
  return (
    <>
      <div className="changebg-container">
        <h2>Navbar <DiAptana/></h2>

        {navbg ? (
            <>
            <AiOutlineArrowLeft
              className="icon"
              onClick={() => setNavBg(false)}
              />{" "}
            <NavBackgrounds />
          </>
        ) : (
            <>
            <h3>Navbar Background</h3>
            <FiEdit
              className="icon"
              onClick={() => setNavBg(true)}
            />
          </>
        )}


        {colorbg ? (
            <>
            <AiOutlineArrowLeft
              className="icon"
              onClick={() => setColorbg(false)}
              />{" "}
            <NavColor />
          </>
        ) : (
            <>
            <h3>Navbar Color</h3>
            <FiEdit
              className="icon"
              onClick={() => setColorbg(true)}
            />
          </>
        )}

          
      <h2>Layout <DiAptana/></h2>

        {layoutBg ? (
            <>
            <AiOutlineArrowLeft
              className="icon"
              onClick={() => setLayoutBg(false)}
              />{" "}
            <LayoutBg />
          </>
        ) : (
            <>
            <h3>Layout Background</h3>
            <FiEdit
              className="icon"
              onClick={() => setLayoutBg(true)}
            />
          </>
        )}



        
        {layoutColor ? (
            <>
            <AiOutlineArrowLeft
              className="icon"
              onClick={() => setLayoutColor(false)}
              />{" "}
            <LayoutColor />
          </>
        ) : (
            <>
            <h3>Layout Colors</h3>
            <FiEdit
              className="icon"
              onClick={() => setLayoutColor(true)}
            />
          </>
        )}


        <h2>Footer <DiAptana/></h2>
        
        {footerBg ? (
            <>
            <AiOutlineArrowLeft
              className="icon"
              onClick={() => setFooterBg(false)}
              />{" "}
            <FooterBg />
          </>
        ) : (
            <>
            <h3>Footer Background</h3>
            <FiEdit
              className="icon"
              onClick={() => setFooterBg(true)}
            />
          </>
        )}


        
        {footerColor ? (
            <>
            <AiOutlineArrowLeft
              className="icon"
              onClick={() => setFooterColor(false)}
              />{" "}
            <FooterColor />
          </>
        ) : (
            <>
            <h3>Footer Colors</h3>
            <FiEdit
              className="icon"
              onClick={() => setFooterColor(true)}
            />
          </>
        )}

      </div>
    </>
  );
};

export default ChangeBg;
