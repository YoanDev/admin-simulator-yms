import { BtnFooterColor } from "../../Btns/Btns";

const FooterColor = () => {
  return (
    <>
      <div className="flex">
        <h3>Change Footer Color</h3>
        <div className="btn-wrapper">
          <BtnFooterColor action="SET_FC_WHITE" text="White" />
          <BtnFooterColor action="SET_FC_YELLOW" text="Yellow" />
          <BtnFooterColor action="SET_FC_RED" text="Red" />
          <BtnFooterColor action="SET_FC_BLUE" text="Blue" />
          <BtnFooterColor action="SET_FC_GREY" text="Grey" />
          <BtnFooterColor action="SET_FC_BLACK" text="Black" />
          <BtnFooterColor action="SET_FC_PINK" text="Pink" />
          <BtnFooterColor action="SET_FC_VIOLET" text="Violet" />
          <BtnFooterColor action="SET_FC_BROWN" text="Brown" />
        </div>
      </div>
    </>
  );
};

export default FooterColor;
