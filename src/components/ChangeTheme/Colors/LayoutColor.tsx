import { BtnLayoutColor } from "../../Btns/Btns";

const LayoutColor = () => {
  return (
    <>
      <div className="flex">
        <h3>Change Layout Background</h3>
        <div className="btn-wrapper">
          <BtnLayoutColor action="SET_LC_WHITE" text="White" />
          <BtnLayoutColor action="SET_LC_YELLOW" text="Yellow" />
          <BtnLayoutColor action="SET_LC_RED" text="Red" />
          <BtnLayoutColor action="SET_LC_BLUE" text="Blue" />
          <BtnLayoutColor action="SET_LC_GREY" text="Grey" />
          <BtnLayoutColor action="SET_LC_BLACK" text="Black" />
          <BtnLayoutColor action="SET_LC_PINK" text="Pink" />
          <BtnLayoutColor action="SET_LC_VIOLET" text="Violet" />
          <BtnLayoutColor action="SET_LC_BROWN" text="Brown" />
        </div>
      </div>
    </>
  );
};

export default LayoutColor;
