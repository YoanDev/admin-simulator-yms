import { BtnNavColor } from "../../Btns/Btns";

const NavColor = () => {
  return (
    <>
      <div className="flex">
        <h3>Change Navbar Color</h3>
        <div className="btn-wrapper">
          <BtnNavColor action="SET_NC_WHITE" text="White" />
          <BtnNavColor action="SET_NC_YELLOW" text="Yellow" />
          <BtnNavColor action="SET_NC_RED" text="Red" />
          <BtnNavColor action="SET_NC_BLUE" text="Blue" />
          <BtnNavColor action="SET_NC_GREY" text="Grey" />
          <BtnNavColor action="SET_NC_BLACK" text="Black" />
          <BtnNavColor action="SET_NC_PINK" text="Pink" />
          <BtnNavColor action="SET_NC_VIOLET" text="Violet" />
          <BtnNavColor action="SET_NC_BROWN" text="Brown" />
        </div>
      </div>
    </>
  );
};

export default NavColor;
