import { useContext } from "react";
import { bgFooterContext, colorFooterContext } from "../../hook/MyContext";
import { FooterContainer, FooterText } from "./FooterEl";

const Footer = () => {
  const { state5 } = useContext(bgFooterContext);
  const { state6 } = useContext(colorFooterContext);

  return (
    <>
      <FooterContainer
        bgfooter={state5.bgfooter}
        colorfooter={state6.colorfooter}
      >
        <FooterText>
          <p>All right deserved ® YMS</p>
          <h4>mentions légales</h4>
        </FooterText>
      </FooterContainer>
    </>
  );
};

export default Footer;
