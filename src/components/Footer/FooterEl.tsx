import styled from "styled-components";

export const FooterContainer = styled.div<any>`
  background: ${({ bgfooter }) => bgfooter && bgfooter};
  color: ${({ colorfooter }) => colorfooter && colorfooter};
  height: 80px;
  width: 100%;
  display: flex;
  position: fixed;
  padding: 20px;
  margin-top: 20px;
  align-items: center;
  justify-content: center;
  bottom: 0;
  box-shadow: rgba(100, 100, 111, 0.2) 0px 7px 29px 0px;
`;
export const FooterText = styled.div<any>`
  display: flex;
  position: relative;
  padding: 20px;
  margin-top: 20px;
  flex-direction: column;
`;
