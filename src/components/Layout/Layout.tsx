import { useContext } from "react";
import { bgLayoutContext, colorLayoutContext } from "../../hook/MyContext";
import { LayoutContainer } from "./LayoutEl";

interface Props {
  children: any;
}

const Layout = ({ children }: Props) => {
    const {state3} = useContext(bgLayoutContext);
    const {state4} = useContext(colorLayoutContext);
  return (
    <>
    
      <LayoutContainer bglayout={state3.bglayout} colorlayout={state4.colorlayout}>{children}</LayoutContainer>
    </>
  );
};

export default Layout;
