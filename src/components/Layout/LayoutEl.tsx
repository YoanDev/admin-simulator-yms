import styled from "styled-components";

export const LayoutContainer = styled.div<any>`
  background: ${({ bglayout }) => bglayout && bglayout};
  color: ${({ colorlayout }) => colorlayout && colorlayout};
  display: flex;
  position: relative;
  min-height: 150vh;
  flex-direction: column;
  box-shadow: rgba(149, 157, 165, 0.2) 0px 8px 24px;
`;
