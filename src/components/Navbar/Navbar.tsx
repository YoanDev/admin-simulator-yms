import { useContext } from "react";
import { bgNavContext, colorNavContext } from "../../hook/MyContext"
import { Nav, NavItems, NavLinks, NavMenu } from "./NavbarEl"


const Navbar = () => {
    const {state} = useContext(bgNavContext);
    const {state2} = useContext(colorNavContext);
    return (
        <>
            <Nav bgnav={state.bgnav}>
                <NavMenu>
                    <NavItems>
                        <NavLinks colornav={state2.colornav} to="/">Home</NavLinks>
                    </NavItems>
                    <NavItems>
                        <NavLinks colornav={state2.colornav} to="/about">About</NavLinks>
                    </NavItems>
                </NavMenu>
            </Nav>
        </>
    )
}

export default Navbar
