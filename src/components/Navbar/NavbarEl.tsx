import styled from 'styled-components'
import { Link as LinkR } from 'react-router-dom'


export const Nav = styled.nav<any>`
 background: ${({bgnav})=> (bgnav && bgnav)};
 display:flex;
 position:fixed;
 width:100%;
 height:80px;
 justify-content:center;
 align-items:center;
 top:0;
 z-index:10;
 font-size:1rem;
 box-shadow: rgba(149, 157, 165, 0.2) 0px 8px 24px;
`

export const NavbarContainer = styled.div`
 display:flex;
 justify-content:space-between;
 height:80px;
 width:100%;
 padding:0 24px;
 max-width:1100px;
`

export const NavMenu = styled.ul`
 display:flex;
 align-items:center;
 text-align:center;
 list-style:none;
 margin-right:-22px;

 @media screen and (max-width:768px){
     display:none;
 }
`

export const NavItems = styled.li`
 height:80px;
`

export const NavLinks = styled(LinkR)<any>`
    color:${({colornav})=> (colornav && colornav)};
    display:flex;
    align-items:center;
    text-decoration:none;
    padding:0 1rem;
    height:100%;
    cursor: pointer;

    &.active{
        border-bottom: 3px solid #FFE724;
    }
`