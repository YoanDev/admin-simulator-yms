import {createContext} from 'react'

interface BgNav {
    state: any,
    dispatch : any
}
export const bgNavContext = createContext({} as BgNav);


interface ColorNav {
    state2: any,
    dispatch : any
}

export const colorNavContext = createContext({} as ColorNav);


interface BgLayout {
    state3: any,
    dispatch : any
}

export const bgLayoutContext = createContext({} as BgLayout);




interface ColorLayout {
    state4: any,
    dispatch : any
}

export const colorLayoutContext = createContext({} as ColorLayout);



interface BgFooter {
    state5: any,
    dispatch : any
}

export const bgFooterContext = createContext({} as BgFooter);



interface ColorFooter {
    state6: any,
    dispatch : any
}

export const colorFooterContext = createContext({} as ColorFooter);