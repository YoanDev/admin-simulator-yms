/* eslint-disable default-case */
import { useReducer } from 'react';

const initialState = {
  bgnav: '#000',
};

const reducer = (state: any, action: any) => {
  switch (action.type) {
    case 'SET_WHITE': return {bgnav: 'white'};
    case 'SET_YELLOW': return {bgnav: 'yellow'};
    case 'SET_BLACK': return {bgnav: 'black'};
    case 'SET_BROWN': return {bgnav: 'brown'};
    case 'SET_GREY': return {bgnav: 'grey'};
    case 'SET_RED': return {bgnav: 'red'};
    case 'SET_BLUE': return {bgnav: 'blue'};
    case 'SET_GREEN': return {bgnav: 'green'};
    case 'SET_VIOLET': return {bgnav: 'VIOLET'};
    case 'SET_PINK': return {bgnav: 'PINK'};
    
    default:
      return {
        bgnav: '#000',
      };
  }
};

export const useBgNavReducer = () => useReducer(reducer, initialState);




const initialState2 = {
  colornav: '#fff'
};

const colorNavReducer = (state: any, action: any) => {
  switch (action.type) {
    case 'SET_NC_WHITE': return {colornav: 'white'};
    case 'SET_NC_YELLOW': return {colornav: 'yellow'};
    case 'SET_NC_BLACK': return {colornav: 'black'};
    case 'SET_NC_BROWN': return {colornav: 'brown'};
    case 'SET_NC_GREY': return {colornav: 'grey'};
    case 'SET_NC_RED': return {colornav: 'red'};
    case 'SET_NC_BLUE': return {colornav: 'blue'};
    case 'SET_NC_GREEN': return {colornav: 'green'};
    case 'SET_NC_VIOLET': return {colornav: 'VIOLET'};
    case 'SET_NC_PINK': return {colornav: 'PINK'};
    
    default:
      return {
        colornav: '#fff',
      };
  }
};

export const useColorNavReducer = () => useReducer(colorNavReducer, initialState2);





const initialState3 = {
  bglayout: '#fff'
};

const bgLayoutReducer = (state: any, action: any) => {
  switch (action.type) {
    case 'SET_LYB_WHITE': return {bglayout: 'WHITE'};
    case 'SET_LYB_YELLOW': return {bglayout: 'YELLOW'};
    case 'SET_LYB_BLACK': return {bglayout: 'BLACK'};
    case 'SET_LYB_BROWN': return {bglayout: 'BROWN'};
    case 'SET_LYB_GREY': return {bglayout: 'GREY'};
    case 'SET_LYB_RED': return {bglayout: 'RED'};
    case 'SET_LYB_BLUE': return {bglayout: 'BLUE'};
    case 'SET_LYB_GREEN': return {bglayout: 'GREEN'};
    case 'SET_LYB_VIOLET': return {bglayout: 'VIOLET'};
    case 'SET_LYB_PINK': return {bglayout: 'PINK'};
    
    default:
      return {
        bglayout: '#fff',
      };
  }
};

export const useBgLayoutReducer = () => useReducer(bgLayoutReducer, initialState3);




const initialState4 = {
  colorlayout: '#000'
};

const colorLayoutReducer = (state: any, action: any) => {
  switch (action.type) {
    case 'SET_LC_WHITE': return {colorlayout: 'WHITE'};
    case 'SET_LC_YELLOW': return {colorlayout: 'YELLOW'};
    case 'SET_LC_BLACK': return {colorlayout: 'BLACK'};
    case 'SET_LC_BROWN': return {colorlayout: 'BROWN'};
    case 'SET_LC_GREY': return {colorlayout: 'GREY'};
    case 'SET_LC_RED': return {colorlayout: 'RED'};
    case 'SET_LC_BLUE': return {colorlayout: 'BLUE'};
    case 'SET_LC_GREEN': return {colorlayout: 'GREEN'};
    case 'SET_LC_VIOLET': return {colorlayout: 'VIOLET'};
    case 'SET_LC_PINK': return {colorlayout: 'PINK'};
    
    default:
      return {
        colorlayout: '#000',
      };
  }
};

export const useColorLayoutReducer = () => useReducer(colorLayoutReducer, initialState4);




const initialState5 = {
  bgfooter: '#000'
};

const bgFooterReducer = (state: any, action: any) => {
  switch (action.type) {
    case 'SET_BFC_WHITE': return {bgfooter: 'WHITE'};
    case 'SET_BFC_YELLOW': return {bgfooter: 'YELLOW'};
    case 'SET_BFC_BLACK': return {bgfooter: 'BLACK'};
    case 'SET_BFC_BROWN': return {bgfooter: 'BROWN'};
    case 'SET_BFC_GREY': return {bgfooter: 'GREY'};
    case 'SET_BFC_RED': return {bgfooter: 'RED'};
    case 'SET_BFC_BLUE': return {bgfooter: 'BLUE'};
    case 'SET_BFC_GREEN': return {bgfooter: 'GREEN'};
    case 'SET_BFC_VIOLET': return {bgfooter: 'VIOLET'};
    case 'SET_BFC_PINK': return {bgfooter: 'PINK'};
    
    default:
      return {
        bgfooter: '#000',
      };
  }
};

export const useBgFooterReducer = () => useReducer(bgFooterReducer, initialState5);




const initialState6 = {
  colorfooter: '#fff'
};

const colorFooterReducer = (state: any, action: any) => {
  switch (action.type) {
    case 'SET_FC_WHITE': return {colorfooter: 'WHITE'};
    case 'SET_FC_YELLOW': return {colorfooter: 'YELLOW'};
    case 'SET_FC_BLACK': return {colorfooter: 'BLACK'};
    case 'SET_FC_BROWN': return {colorfooter: 'BROWN'};
    case 'SET_FC_GREY': return {colorfooter: 'GREY'};
    case 'SET_FC_RED': return {colorfooter: 'RED'};
    case 'SET_FC_BLUE': return {colorfooter: 'BLUE'};
    case 'SET_FC_GREEN': return {colorfooter: 'GREEN'};
    case 'SET_FC_VIOLET': return {colorfooter: 'VIOLET'};
    case 'SET_FC_PINK': return {colorfooter: 'PINK'};
    
    default:
      return {
        colorfooter: '#fff',
      };
  }
};

export const useColorFooterReducer = () => useReducer(colorFooterReducer, initialState6);
