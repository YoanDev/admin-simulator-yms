import Footer from "../components/Footer/Footer"
import Layout from "../components/Layout/Layout"
import Navbar from "../components/Navbar/Navbar"

const About = () => {
    return (
        <Layout>
            <Navbar />
            About
            <Footer />
        </Layout>
    )
}

export default About
