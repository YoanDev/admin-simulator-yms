import React from 'react';
import ChangeBg from '../components/ChangeTheme/ChangeBg';
import Footer from '../components/Footer/Footer';
import Layout from '../components/Layout/Layout';
import Navbar from '../components/Navbar/Navbar';

const Home = () => {
    return (
        <Layout>
            <Navbar/>
            <ChangeBg />
            <Footer/>
        </Layout>
    )
}

export default Home
